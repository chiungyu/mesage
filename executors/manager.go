package executors

import (
	"fmt"
	"io"
	"log"

	"gitlab.com/chiungyu/mesage/commons"

	"gopkg.in/yaml.v2"

	"gitlab.com/chiungyu/mesage/executors/executor"
	hubIO "gitlab.com/chiungyu/mesage/io"
	"gitlab.com/chiungyu/mesage/services"
)

// Manager manages the collection of station executors
type Manager struct {
	executors map[string]executor.Executor
}

func buildExecutor(configReader io.Reader, hubMan *hubIO.Manager, serviceMan *services.Manager) (executor.Executor, error) {
	var metadata commons.ConfigMetadata

	dec := yaml.NewDecoder(configReader)
	dec.SetStrict(true)
	if err := dec.Decode(&metadata); err != nil {
		return nil, fmt.Errorf("failed to parse executor config: %w", err)
	}

	// Build the hub, Build will read until error or EOF
	log.Printf("building executor %s with type %s\n", metadata.Name, metadata.Type)
	e, err := Build(metadata.Name, metadata.Type, dec.Decode, hubMan, serviceMan)
	if err != nil {
		return nil, fmt.Errorf("failed to build executor: %w", err)
	}

	return e, nil
}

// NewManager returns a new executor manager
func NewManager(configReaders <-chan io.Reader, hubMan *hubIO.Manager, serviceMan *services.Manager) (*Manager, error) {
	man := &Manager{
		executors: make(map[string]executor.Executor),
	}
	for r := range configReaders {
		e, err := buildExecutor(r, hubMan, serviceMan)
		if err != nil {
			return nil, err
		}

		name := e.Name()
		if _, ok := man.executors[name]; ok {
			return nil, fmt.Errorf("executor name collision [%s]", name)
		}
		man.executors[name] = e
	}

	return man, nil
}

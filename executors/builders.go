package executors

import (
	"fmt"
	"io"
	"log"
	"reflect"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/executors/executor"
	hubIO "gitlab.com/chiungyu/mesage/io"
	"gitlab.com/chiungyu/mesage/io/hub"
	"gitlab.com/chiungyu/mesage/services"
	"gitlab.com/chiungyu/mesage/services/service"
)

// ServicesConfig for mapping registered services to referenced services.
// E.g., a printer service may be referenced as "printer" in an executor implementation,
// while the service may be registered to services manager as "3rd Floor Printer."
type ServicesConfig map[string]string

// ServicesSection for specifying services required by the executor
type ServicesSection struct {
	ServicesConfig ServicesConfig `yaml:"services"`
}

// IOHubHeader for a set of IOs through a hub
type IOHubHeader struct {
	HubName string `yaml:"hub"`
}

var builderRegistry map[string]executor.Builder = map[string]executor.Builder{}

// ClearBuilders clears registered builders
func ClearBuilders() {
	builderRegistry = map[string]executor.Builder{}
}

// RegisterBuilderOrDie registers an executor builder or dies.
// This is expected to be called by init functions of builder implementations.
func RegisterBuilderOrDie(name string, builder executor.Builder) {
	if _, ok := builderRegistry[name]; ok {
		log.Panicf("executor builder name collision [%s]", name)
	}
	if builder == nil {
		log.Panicf("nil executor builder [%s]", name)
	}
	builderRegistry[name] = builder
}

// Caller should provide non-nil parseConfig.
func parseServiceMapping(executorName string, parseConfig commons.ConfigParser, serviceMan *services.Manager) (map[string]service.Service, error) {
	var ss ServicesSection
	if err := parseConfig(&ss); err != nil {
		return nil, err
	}
	if len(ss.ServicesConfig) == 0 {
		return nil, nil
	}

	if serviceMan == nil {
		return nil, fmt.Errorf("nil service manager")
	}
	services := make(map[string]service.Service, len(ss.ServicesConfig))
	for ref, reg := range ss.ServicesConfig {
		s := serviceMan.Service(reg)
		if s == nil {
			return nil, fmt.Errorf("missing service [%s]", reg)
		}
		log.Printf("executor [%s] is using registered service [%s] as its [%s]\n", executorName, reg, ref)
		services[ref] = s
	}
	return services, nil
}

func ioConfigSlice(handleFuncConfigTypes map[string]reflect.Type, ioConfigType reflect.Type) interface{} {
	fields := []reflect.StructField{
		{Name: "Name", Type: reflect.TypeOf(string("")), Tag: `yaml:"name"`},
		{Name: "Func", Type: reflect.TypeOf(string("")), Tag: `yaml:"func"`},
		{Name: "HubIO", Type: ioConfigType, Tag: `yaml:"hubIOConfig"`}}

	for funcName, configType := range handleFuncConfigTypes {
		if configType != nil && len(funcName) > 0 {
			fields = append(fields, reflect.StructField{
				Name: "Func" + funcName,
				Type: configType,
				Tag:  reflect.StructTag(fmt.Sprintf(`yaml:"funcConfig-%s"`, funcName)),
			})
		}
	}
	return reflect.New(reflect.SliceOf(reflect.StructOf(fields))).Interface()
}

// Caller is responsible for making sure non-nil input params.
func parseAndRegisterHubIOs(e executor.Executor, parseConfig commons.ConfigParser, hubMan *hubIO.Manager) error {
	var header IOHubHeader
	if err := parseConfig(&header); err != nil {
		if err == io.EOF { // No more hubs to register
			return err
		}
		return fmt.Errorf("failed to parse IO hub header for executor [%s]: %w", e.Name(), err)
	}
	if len(header.HubName) == 0 {
		return fmt.Errorf("empty IO hub name for executor [%s]", e.Name())
	}

	newIOConfig := hubMan.NewIOConfig(header.HubName)
	if newIOConfig == nil {
		return fmt.Errorf("reigistering executor [%s] IO through non-existent hub [%s]", e.Name(), header.HubName)
	}

	HandleFuncConfigTypes := e.HandleFuncSpecs()
	configSlicePointer := ioConfigSlice(HandleFuncConfigTypes, reflect.TypeOf(newIOConfig()))
	if err := parseConfig(configSlicePointer); err != nil {
		return fmt.Errorf("failed to parse hub [%s] IO configs for executor [%s]: %w", header.HubName, e.Name(), err)
	}
	configSlice := reflect.ValueOf(configSlicePointer).Elem()
	numIOs := configSlice.Len()
	if numIOs == 0 {
		return fmt.Errorf("zero IOs for executor [%s] and IO hub [%s]", e.Name(), header.HubName)
	}
	infoList := make([]*hub.IORegistrationInfo, numIOs)
	for i := 0; i < numIOs; i++ {
		io := configSlice.Index(i)

		funcName, _ := io.FieldByName("Func").Interface().(string)
		funcConfigType, ok := HandleFuncConfigTypes[funcName]
		if !ok {
			return fmt.Errorf("non-existent handle func [%s] for executor [%s] and IO hub [%s]", funcName, e.Name(), header.HubName)
		}
		var handleFuncConfig interface{}
		if funcConfigType != nil {
			handleFuncConfig = io.FieldByName("Func" + funcName).Interface()
		}
		handleFunc, err := e.HandleFunc(funcName, handleFuncConfig)
		if err != nil {
			return fmt.Errorf("failed to get or configure executor-func [%s]-[%s]: %w", e.Name(), funcName, err)
		}

		ioName, _ := io.FieldByName("Name").Interface().(string)
		infoList[i] = &hub.IORegistrationInfo{
			Name:     ioName,
			Executor: e.Name(),
			Function: funcName,
			Handle:   handleFunc,
			Config:   io.FieldByName("HubIO").Interface(),
		}
	}

	log.Printf("registering executor [%s] handler functions to IO hub [%s]\n", e.Name(), header.HubName)
	if err := hubMan.Register(header.HubName, infoList); err != nil {
		return fmt.Errorf("failed to register executor [%s] handle functions to IO hub [%s]: %w", e.Name(), header.HubName, err)
	}
	return nil
}

// Build builds an executor with the named builder the provided config parser.
// IO configs will also be parsed and registered.
func Build(
	executorName, builderName string,
	parseConfig commons.ConfigParser,
	hubMan *hubIO.Manager,
	serviceMan *services.Manager) (executor.Executor, error) {
	if parseConfig == nil {
		return nil, fmt.Errorf("nil config parser for executor [%s]", executorName)
	}

	builder, ok := builderRegistry[builderName]
	if !ok || builder == nil {
		return nil, fmt.Errorf("non-existent executor builder [%s]", builderName)
	}

	// Parse service mapping for required services
	services, err := parseServiceMapping(executorName, parseConfig, serviceMan)
	if err != nil {
		return nil, fmt.Errorf("error parsing or mapping executor [%s] services: %w", executorName, err)
	}

	// Executor main config
	config := builder.NewConfig()
	if err := parseConfig(config); err != nil {
		return nil, fmt.Errorf("error parsing executor [%s] config with builder [%s]: %w", executorName, builderName, err)
	}

	// Build the executor
	executor, err := builder.Build(executorName, config, services)
	if err != nil {
		return nil, fmt.Errorf("failed to build executor [%s] with builder [%s]: %w", executorName, builderName, err)
	}

	// Parse and register hub IOs
	if hubMan == nil {
		return nil, fmt.Errorf("nil hub manager")
	}
	numHubs := 0
	err = parseAndRegisterHubIOs(executor, parseConfig, hubMan)
	for err == nil {
		numHubs++
		err = parseAndRegisterHubIOs(executor, parseConfig, hubMan)
	}
	if err != io.EOF {
		return nil, err
	}
	if numHubs == 0 {
		return nil, fmt.Errorf("no hub IO for executor [%s]", executor.Name())
	}

	return executor, nil
}

package testutils

import (
	"context"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/executors/executor"
	"gitlab.com/chiungyu/mesage/services/service"
)

// MockExecutorType for tests
const MockExecutorType = "mock"

// MockBuilder for building MockExecutor
type MockBuilder int

// MockHandleFuncs for tests
type MockHandleFuncs map[string]string

// MockExecutorConfig for building MockExecutor
type MockExecutorConfig struct {
	Value       uint32          `yaml:"value"`
	Services    string          `yaml:"services"`
	BuildError  string          `yaml:"error"`
	HandleFuncs MockHandleFuncs `yaml:"handleFuncs"`
}

// NewConfig implemetation
func (*MockBuilder) NewConfig() interface{} {
	return new(MockExecutorConfig)
}

// MockFuncUint32Config for configurable handle functions
type MockFuncUint32Config struct {
	Value       uint32 `yaml:"uintVal"`
	Error       string `yaml:"error"`
	ConfigError string `yaml:"configError"`
}

// MockFuncUint32ConfigType for configurable handle functions
var MockFuncUint32ConfigType = reflect.TypeOf(new(MockFuncUint32Config))

// MockFuncStringConfig for configurable handle functions
type MockFuncStringConfig struct {
	Value       string `yaml:"strVal"`
	Error       string `yaml:"error"`
	ConfigError string `yaml:"configError"`
}

// MockFuncStringConfigType for configurable handle functions
var MockFuncStringConfigType = reflect.TypeOf(new(MockFuncStringConfig))

// Build implementation
func (*MockBuilder) Build(name string, config interface{}, services map[string]service.Service) (executor.Executor, error) {
	c, ok := config.(*MockExecutorConfig)
	if !ok || c == nil {
		return nil, commons.ErrorBadConfigType
	}

	if len(c.BuildError) > 0 {
		return nil, fmt.Errorf(c.BuildError)

	}
	if len(c.Services) > 0 {
		for _, name := range strings.Split(c.Services, ",") {
			if services[name] == nil {
				return nil, fmt.Errorf("missing required service [%s]", name)
			}
		}
	}

	handleFuncs := make(map[string]reflect.Type, len(c.HandleFuncs))
	for funcName, configTypeString := range c.HandleFuncs {
		switch configTypeString {
		case "noconf":
			handleFuncs[funcName] = nil
		case "uint32":
			handleFuncs[funcName] = MockFuncUint32ConfigType
		case "string":
			handleFuncs[funcName] = MockFuncStringConfigType
		}
	}
	return &MockExecutor{name: name, handleFuncs: handleFuncs}, nil
}

// MockExecutor for tests
type MockExecutor struct {
	name        string
	handleFuncs map[string]reflect.Type
}

// Name implementation
func (m *MockExecutor) Name() string {
	return m.name
}

// Type implementation
func (m *MockExecutor) Type() string {
	return MockExecutorType
}

// HandleFuncSpecs implementation
func (m *MockExecutor) HandleFuncSpecs() map[string]reflect.Type {
	return m.handleFuncs
}

func mockHandleFunc(value interface{}, err, configError string) (commons.Handle, error) {
	if len(configError) > 0 {
		return nil, fmt.Errorf(configError)
	}
	return func(context.Context, commons.Values) (commons.Values, error) {
		if len(err) > 0 {
			return nil, fmt.Errorf(err)
		}
		return commons.Values{KeyConfigValue: value}, nil
	}, nil
}

// Constants for HandleFunc
const (
	ErrorNoSuchConfigurableFunc = "no such configurable func"
	ErrorBadConfigType          = "bad config type"
	KeyConfigValue              = "configValue"
)

// HandleFunc implementation
func (m *MockExecutor) HandleFunc(funcName string, config interface{}) (commons.Handle, error) {
	configType, ok := m.handleFuncs[funcName]
	if !ok {
		return nil, fmt.Errorf(ErrorNoSuchConfigurableFunc)
	}

	switch configType {
	case nil:
		if config != nil {
			return nil, fmt.Errorf(ErrorBadConfigType)
		}
		return func(context.Context, commons.Values) (commons.Values, error) { return nil, nil }, nil
	case MockFuncUint32ConfigType:
		if c, ok := config.(*MockFuncUint32Config); ok && c != nil {
			return mockHandleFunc(c.Value, c.Error, c.ConfigError)
		}
	case MockFuncStringConfigType:
		if c, ok := config.(*MockFuncStringConfig); ok && c != nil {
			return mockHandleFunc(c.Value, c.Error, c.ConfigError)
		}
	}
	return nil, fmt.Errorf(ErrorBadConfigType)
}

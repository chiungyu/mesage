package testutils

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/executors/executor"
	"gitlab.com/chiungyu/mesage/services/service"
	servicesTestutils "gitlab.com/chiungyu/mesage/services/testutils"
)

func TestMock(t *testing.T) {
	builder := new(MockBuilder)
	assert.Implements(t, new(executor.Builder), builder)

	// Good cases
	{
		config, ok := builder.NewConfig().(*MockExecutorConfig)
		assert.True(t, ok)

		config.HandleFuncs = MockHandleFuncs{
			"testNoConfFunc": "noconf",
			"testUint32Func": "uint32",
			"testStringFunc": "string",
		}
		e, err := builder.Build("test", config, nil)
		assert.NoError(t, err)
		assert.Implements(t, new(executor.Executor), e)

		assert.Equal(t, "test", e.Name())
		assert.Equal(t, "mock", e.Type())

		assert.Equal(t, map[string]reflect.Type{
			"testNoConfFunc": nil,
			"testUint32Func": MockFuncUint32ConfigType,
			"testStringFunc": MockFuncStringConfigType,
		}, e.HandleFuncSpecs())

		{ // non-existent func
			f, err := e.HandleFunc("nonExTestFunc", nil)
			assert.EqualError(t, err, ErrorNoSuchConfigurableFunc)
			assert.Nil(t, f)
		}

		// Existent funcs
		{
			f, err := e.HandleFunc("testNoConfFunc", nil)
			assert.NoError(t, err)
			assert.NotNil(t, f)
			_, err = f(nil, nil)
			assert.NoError(t, err)
		}
		// Existent funcs
		{
			f, err := e.HandleFunc("testUint32Func", &MockFuncUint32Config{Value: 50})
			assert.NoError(t, err)
			assert.NotNil(t, f)
			v, err := f(nil, nil)
			assert.NoError(t, err)
			assert.Equal(t, v, commons.Values{KeyConfigValue: uint32(50)})
		}
		{
			f, err := e.HandleFunc("testStringFunc", &MockFuncStringConfig{Value: "tess"})
			assert.NoError(t, err)
			assert.NotNil(t, f)
			v, err := f(nil, nil)
			assert.NoError(t, err)
			assert.Equal(t, v, commons.Values{KeyConfigValue: "tess"})
		}
		// Existent funcs error paths
		{
			f, err := e.HandleFunc("testUint32Func", &MockFuncUint32Config{Error: "some error"})
			assert.NoError(t, err)
			assert.NotNil(t, f)
			v, err := f(nil, nil)
			assert.EqualError(t, err, "some error")
			assert.Nil(t, v)
		}
		{
			f, err := e.HandleFunc("testUint32Func", &MockFuncUint32Config{ConfigError: "some config error"})
			assert.EqualError(t, err, "some config error")
			assert.Nil(t, f)
		}
		{
			f, err := e.HandleFunc("testStringFunc", &MockFuncUint32Config{Value: 50}) // string vs. uint32
			assert.EqualError(t, err, ErrorBadConfigType)
			assert.Nil(t, f)
		}
		{
			f, err := e.HandleFunc("testNoConfFunc", &MockFuncUint32Config{Value: 50}) // non-configurable vs. uint32
			assert.EqualError(t, err, ErrorBadConfigType)
			assert.Nil(t, f)
		}
		{
			var nilConf *MockFuncUint32Config
			f, err := e.HandleFunc("testUint32Func", nilConf)
			assert.EqualError(t, err, ErrorBadConfigType)
			assert.Nil(t, f)
		}
	}

	// Bad config objects
	{
		_, err := builder.Build("test", nil, nil)
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}
	{
		_, err := builder.Build("test", new(int), nil)
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}

	// Build error
	{
		config, ok := builder.NewConfig().(*MockExecutorConfig)
		assert.True(t, ok)

		config.BuildError = "some error"
		_, err := builder.Build("test", config, nil)
		assert.EqualError(t, err, "some error")
	}

	// Missing required services
	{
		config, ok := builder.NewConfig().(*MockExecutorConfig)
		assert.True(t, ok)

		config.Services = "exService,nonExService"
		_, err := builder.Build("test", config, map[string]service.Service{"exService": new(servicesTestutils.MockService)})
		assert.EqualError(t, err, "missing required service [nonExService]")
	}
}

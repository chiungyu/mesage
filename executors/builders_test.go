package executors

import (
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/executors/executor"
	"gitlab.com/chiungyu/mesage/executors/testutils"
	hubIO "gitlab.com/chiungyu/mesage/io"
	ioTestutils "gitlab.com/chiungyu/mesage/io/testutils"
	"gitlab.com/chiungyu/mesage/services"
	servicesTestutils "gitlab.com/chiungyu/mesage/services/testutils"
	"gopkg.in/yaml.v2"
)

func TestRegisterExecutorBuilder(t *testing.T) {
	{ // nil builder
		assert.PanicsWithValue(t,
			"nil executor builder [nilBuilder]",
			func() { RegisterBuilderOrDie("nilBuilder", nil) })
		assert.Equal(t, map[string]executor.Builder{}, builderRegistry)
	}

	{
		// normal register calls
		assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })
		assert.NotPanics(t, func() { RegisterBuilderOrDie("mockAlt", new(testutils.MockBuilder)) })
		assert.Equal(t, 2, len(builderRegistry))
		assert.NotNil(t, builderRegistry["mock"])
		assert.NotNil(t, builderRegistry["mockAlt"])

		// builder name collision
		assert.PanicsWithValue(t,
			"executor builder name collision [mock]",
			func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

		ClearBuilders()
	}
}

func TestBuildExecutor(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

	defer hubIO.ClearBuilders()
	assert.NotPanics(t, func() { hubIO.RegisterHubBuilderOrDie("hub", new(ioTestutils.MockBuilder)) })
	hubManConfigReaders := make(chan io.Reader, 2)
	hubManConfigReaders <- strings.NewReader(`
---
name: hub0
type: hub
---
value: 0
`)
	hubManConfigReaders <- strings.NewReader(`
---
name: hub1
type: hub
---
value: 5
`)
	close(hubManConfigReaders)
	hubMan, err := hubIO.NewManager(hubManConfigReaders)
	assert.NoError(t, err)
	assert.NotNil(t, hubMan)

	defer services.ClearBuilders()
	assert.NotPanics(t, func() { services.RegisterBuilderOrDie("service", new(servicesTestutils.MockBuilder)) })
	serviceManConfigReaders := make(chan io.Reader, 2)
	serviceManConfigReaders <- strings.NewReader(`
---
name: s0
type: service
---
value: 7
`)
	serviceManConfigReaders <- strings.NewReader(`
---
name: s1
type: service
---
value: 2
`)
	close(serviceManConfigReaders)
	serviceMan, err := services.NewManager(serviceManConfigReaders)
	assert.NoError(t, err)
	assert.NotNil(t, serviceMan)

	goodConfig := `
---
services: {ref0: s0, ref1: s1}
---
services: ref0,ref1
handleFuncs: {func0: "noconf", func1: "noconf"}
---
hub: hub0
---
- name: func0
  func: func0
  hubIOConfig: { value: 5 }
- name: func1
  func: func1
  hubIOConfig: { value: 10 }
---
hub: hub1
---
- # name: func0 # name is optional
  func: func0
  hubIOConfig:
    value: 5
`

	{
		configParser := yaml.NewDecoder(strings.NewReader(goodConfig)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.NoError(t, err)
		assert.NotNil(t, e)
		assert.Equal(t, "testExecutor", e.Name())
		assert.Equal(t, testutils.MockExecutorType, e.Type())
	}

	// nil build inputs
	{
		e, err := Build("testExecutor", "mock", nil, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "nil config parser for executor [testExecutor]")
	}
	{
		configParser := yaml.NewDecoder(strings.NewReader(goodConfig)).Decode
		e, err := Build("testExecutor", "mock", configParser, nil, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "nil hub manager")
	}
	{
		configParser := yaml.NewDecoder(strings.NewReader(goodConfig)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, nil)
		assert.Nil(t, e)
		assert.EqualError(t, err, "error parsing or mapping executor [testExecutor] services: nil service manager")
	}

	// missing hub IOs
	{
		config := `
---
services: {}
---
# not using services
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "no hub IO for executor [testExecutor]")
	}

	// service mapping tests
	{ // not using any services is okay
		config := `
---
services: {}
---
# not using services
handleFuncs: {func0: "noconf", func1: "noconf"}
---
hub: hub0
---
- name: func0
  func: func0
  hubIOConfig: { value: 10 }
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.NotNil(t, e)
		assert.NoError(t, err)
	}
	{ // missing required service
		config := `
---
services: {ref0: NonEx} # not a registered service
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "error parsing or mapping executor [testExecutor] services: missing service [NonEx]")
	}
	{ // bad service mapping section
		config := `
---
something: else
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "error parsing or mapping executor [testExecutor] services"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	{ // missing required service
		config := `
---
services: {ref0: s0}
---
services: NonEx # not in services mapping section
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "failed to build executor [testExecutor] with builder [mock]: missing required service [NonEx]")
	}

	{ // build error
		config := `
---
services: {ref0: s0}
---
error: "some build error" 
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err,
			"failed to build executor [testExecutor] with builder [mock]: some build error")
	}

	{ // missing executor builder
		configParser := yaml.NewDecoder(strings.NewReader(goodConfig)).Decode
		e, err := Build("testExecutor", "nonExistent", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "non-existent executor builder [nonExistent]")
	}

	{
		config := `
---
services: {ref0: s0}
---
no: such field in executor config 
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "error parsing executor [testExecutor] config with builder [mock]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	// hub io registration tests
	{ // bad hub header
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
no: such field in hub header
---
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "failed to parse IO hub header for executor [testExecutor]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}
	{ // empty hub name
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: ""
---
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "empty IO hub name for executor [testExecutor]")
	}
	{ // no ios for a hub
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: "hub0"
---
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "zero IOs for executor [testExecutor] and IO hub [hub0]")
	}
	{ // registration error from mock
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: hub0
---
- func: func1
  hubIOConfig: 
    error: some error
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "failed to register executor [testExecutor] handle functions to IO hub [hub0]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}
	{ // no such hub
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: NonEx
---
- func: func1
  hubIOConfig: 
    value: 10
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "reigistering executor [testExecutor] IO through non-existent hub [NonEx]")
	}
	{ // bad IO config
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: hub0
---
- func: func1
  no: such field in IO header
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "failed to parse hub [hub0] IO configs for executor [testExecutor]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	{ // non-existent handle func
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: hub0
---
- func: func1
  hubIOConfig: { value: 10 }
- func: funcNonEx
  hubIOConfig: { value: 10 }
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "non-existent handle func [funcNonEx] for executor [testExecutor] and IO hub [hub0]")
	}
	{ // bad IO config
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {func0: "", func1: "noconf"}
---
hub: hub0
---
- name: func1GoodConfig
  func: func1
  ioHubConfig: { value: 10 }
- name: func1BadConfig
  func: func1
  ioHubConfig:
    value: 10
    no: such config field for hub0 IOs
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "failed to parse hub [hub0] IO configs for executor [testExecutor]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	// Configurable handle func cases
	goodConfigWithConfigurableFuncs := `
---
services: {ref0: s0, ref1: s1}
---
services: ref0,ref1
handleFuncs: {testNoConfFunc: noconf, testUint32Func: uint32, testStringFunc: string}
---
hub: hub0
---
- func: testNoConfFunc
  hubIOConfig: { value: 10 }
- func: testUint32Func
  funcConfig-testUint32Func: { value: 3 }
  hubIOConfig: { value: 5 }
---
hub: hub1
---
- func: testStringFunc
  funcConfig-testStringFunc: { value: strVal }
  hubIOConfig: { value: 42 }
`
	{
		configParser := yaml.NewDecoder(strings.NewReader(goodConfigWithConfigurableFuncs)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.NoError(t, err)
		assert.NotNil(t, e)
		assert.Equal(t, "testExecutor", e.Name())
		assert.Equal(t, testutils.MockExecutorType, e.Type())
	}
	{
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {testNoConfFunc: noconf, testUint32Func: uint32, testStringFunc: string}
---
hub: hub0
---
- func: testStringFunc
  funcConfig-testStringFunc: { no: "such field in string func config" }
  hubIOConfig: { value: 42 }
`
		dec := yaml.NewDecoder(strings.NewReader(config))
		dec.SetStrict(true)
		configParser := dec.Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		es := "failed to parse hub [hub0] IO configs for executor [testExecutor]"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}
	{
		config := `
---
services: {ref0: s0}
---
services: ref0
handleFuncs: {testNoConfFunc: noconf, testUint32Func: uint32, testStringFunc: string}
---
hub: hub0
---
- func: testStringFunc
  funcConfig-testStringFunc: { configError: "some config error" }
  hubIOConfig: { value: 42 }
`
		configParser := yaml.NewDecoder(strings.NewReader(config)).Decode
		e, err := Build("testExecutor", "mock", configParser, hubMan, serviceMan)
		assert.Nil(t, e)
		assert.EqualError(t, err, "failed to get or configure executor-func [testExecutor]-[testStringFunc]: some config error")
	}
}

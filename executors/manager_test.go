package executors

import (
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/executors/testutils"
	hubIO "gitlab.com/chiungyu/mesage/io"
	ioTestutils "gitlab.com/chiungyu/mesage/io/testutils"
	"gitlab.com/chiungyu/mesage/services"
	servicesTestutils "gitlab.com/chiungyu/mesage/services/testutils"
)

func TestNewManager(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

	defer hubIO.ClearBuilders()
	assert.NotPanics(t, func() { hubIO.RegisterHubBuilderOrDie("hub", new(ioTestutils.MockBuilder)) })
	hubManConfigReaders := make(chan io.Reader, 2)
	hubManConfigReaders <- strings.NewReader(`
---
name: hub0
type: hub
---
value: 0
`)
	hubManConfigReaders <- strings.NewReader(`
---
name: hub1
type: hub
---
value: 5
`)
	close(hubManConfigReaders)
	hubMan, err := hubIO.NewManager(hubManConfigReaders)
	assert.NoError(t, err)
	assert.NotNil(t, hubMan)

	defer services.ClearBuilders()
	assert.NotPanics(t, func() { services.RegisterBuilderOrDie("service", new(servicesTestutils.MockBuilder)) })
	serviceManConfigReaders := make(chan io.Reader, 2)
	serviceManConfigReaders <- strings.NewReader(`
---
name: s0
type: service
---
value: 7
`)
	serviceManConfigReaders <- strings.NewReader(`
---
name: s1
type: service
---
value: 2
`)
	close(serviceManConfigReaders)
	serviceMan, err := services.NewManager(serviceManConfigReaders)
	assert.NoError(t, err)
	assert.NotNil(t, serviceMan)

	{ // empty config
		configReaders := make(chan io.Reader, 1)
		close(configReaders)

		man, err := NewManager(configReaders, hubMan, serviceMan)
		assert.NoError(t, err)
		assert.NotNil(t, man)
	}

	goodExecutorConfig := `
---
services: {ref0: s0, ref1: s1}
---
services: ref0,ref1
handleFuncs: {func0: "noconf", func1: "noconf"}
---
hub: hub0
---
- name: func0
  func: func0
  hubIOConfig: { value: 5 }
- name: func1
  func: func1
  hubIOConfig: { value: 10 }
---
hub: hub1
---
- # name: func0 # name is optional
  func: func0
  hubIOConfig:
    value: 5
`

	{ // good case
		executorNames := []string{"e0", "e1", "e2"}
		configReaders := make(chan io.Reader, len(executorNames))
		for _, executorName := range executorNames {
			configReaders <- strings.NewReader(
				fmt.Sprintf("---\nname: %s\ntype: mock\n%s", executorName, goodExecutorConfig))
		}
		close(configReaders)

		man, err := NewManager(configReaders, hubMan, serviceMan)
		assert.NoError(t, err)
		assert.NotNil(t, man)

		for _, executorName := range executorNames {
			e, ok := man.executors[executorName].(*testutils.MockExecutor)
			assert.True(t, ok)
			assert.NotNil(t, e)
			assert.Equal(t, executorName, e.Name())
			assert.Equal(t, testutils.MockExecutorType, e.Type())
		}
	}

	{ // bad executor metadata
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader("---\nbad: metadata\n")
		close(configReaders)

		man, err := NewManager(configReaders, hubMan, serviceMan)
		assert.Nil(t, man)
		es := "failed to parse executor config"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	{ // bad executor config
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(
			"---\nname: testExecutor\ntype: mock\n" +
				"---\nvalue: -6 # expecting uint\n")
		close(configReaders)

		man, err := NewManager(configReaders, hubMan, serviceMan)
		assert.Nil(t, man)
		es := "failed to build executor"
		assert.Error(t, err)
		assert.Equal(t, es, err.Error()[:len(es)])
	}

	{ // executor name collision
		configReaders := make(chan io.Reader, 2)
		configReaders <- strings.NewReader("---\nname: dupe\ntype: mock\n" + goodExecutorConfig)
		configReaders <- strings.NewReader("---\nname: dupe\ntype: mock\n" + goodExecutorConfig)
		close(configReaders)

		man, err := NewManager(configReaders, hubMan, serviceMan)
		assert.EqualError(t, err, "executor name collision [dupe]")
		assert.Nil(t, man)
	}
}

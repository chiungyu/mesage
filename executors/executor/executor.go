package executor

import (
	"reflect"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/services/service"
)

// Executor handles requests from IO hubs.
type Executor interface {
	Name() string // Name of the executor
	Type() string // Type (builder name) of the executor

	HandleFuncSpecs() map[string]reflect.Type               // funcName -> reflect.Type(new(funcConfig)); nil if not configurable
	HandleFunc(string, interface{}) (commons.Handle, error) // Handle func getter
}

// Builder for building executors
type Builder interface {
	commons.Configer
	Build(name string, config interface{}, services map[string]service.Service) (Executor, error)
}

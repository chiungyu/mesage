package io

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/chiungyu/mesage/io/hub"
	"gitlab.com/chiungyu/mesage/io/testutils"
)

func TestRegisterHubBuilder(t *testing.T) {
	{ // nil builder
		assert.PanicsWithValue(t,
			"nil IO hub builder [nilBuilder]",
			func() { RegisterHubBuilderOrDie("nilBuilder", nil) })
		assert.Equal(t, map[string]hub.Builder{}, hubBuilderRegistry)
	}

	{
		// normal register calls
		assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })
		assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mockAlt", new(testutils.MockBuilder)) })
		assert.Equal(t, 2, len(hubBuilderRegistry))
		assert.NotNil(t, hubBuilderRegistry["mock"])
		assert.NotNil(t, hubBuilderRegistry["mockAlt"])

		// builder name collision
		assert.PanicsWithValue(t,
			"IO hub builder name collision [mock]",
			func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })

		ClearBuilders()
	}
}

func TestBuildHub(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })

	{
		configParser := func(interface{}) error { return nil }
		hub, err := BuildHub("testHub", "mock", configParser)
		assert.NoError(t, err)
		assert.NotNil(t, hub)
		assert.Equal(t, "testHub", hub.Name())
		assert.Equal(t, testutils.MockHubType, hub.Type())
	}

	{
		buildError := "some build error"
		configParser := func(c interface{}) error {
			config, ok := c.(*testutils.MockHubConfig)
			assert.True(t, ok)
			assert.NotNil(t, config)
			config.BuildError = buildError
			return nil
		}

		hub, err := BuildHub("testHub", "mock", configParser)
		assert.Nil(t, hub)
		assert.EqualError(t, err,
			fmt.Sprintf("failed to build hub [testHub] with builder [mock]: %s", buildError))
	}

	{
		hub, err := BuildHub("testHub", "mock", nil)
		assert.Nil(t, hub)
		assert.EqualError(t, err, "nil config parser for hub [testHub]")
	}

	{
		configError := "some config error"
		configParser := func(c interface{}) error {
			return fmt.Errorf(configError)
		}

		hub, err := BuildHub("testHub", "mock", configParser)
		assert.Nil(t, hub)
		assert.EqualError(t, err,
			fmt.Sprintf("error parsing hub [testHub] config with builder [mock]: %s", configError))
	}

	{
		configParser := func(interface{}) error { return nil }
		hub, err := BuildHub("testHub", "nonExistent", configParser)
		assert.Nil(t, hub)
		assert.EqualError(t, err, "non-existent IO hub builder [nonExistent]")
	}
}

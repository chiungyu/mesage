package io

import (
	"context"
	"fmt"
	"io"
	"log"
	"sync"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/io/hub"
	"gopkg.in/yaml.v2"
)

// Manager manages the collection of station IO hubs
type Manager struct {
	hubs map[string]hub.Hub
}

func buildHub(configReader io.Reader) (hub.Hub, error) {
	if configReader == nil {
		return nil, fmt.Errorf("nil hub config reader")
	}

	// Parse config from reader
	var metadata commons.ConfigMetadata

	dec := yaml.NewDecoder(configReader)
	dec.SetStrict(true)
	if err := dec.Decode(&metadata); err != nil {
		return nil, fmt.Errorf("failed to parse IO hub config: %w", err)
	}

	// Build the hub
	log.Printf("building io hub [%s] with type [%s]\n", metadata.Name, metadata.Type)
	h, err := BuildHub(metadata.Name, metadata.Type, dec.Decode)
	if err != nil {
		return nil, fmt.Errorf("failed to build IO hub: %w", err)
	}

	// Expect EOF - one hub config per reader
	if err := dec.Decode(&metadata); err != io.EOF {
		return nil, fmt.Errorf("unexpected config input, expecting EOF")
	}

	return h, nil
}

// NewManager returns a new hub manager
func NewManager(configReaders <-chan io.Reader) (*Manager, error) {
	if configReaders == nil {
		return nil, fmt.Errorf("nil hub config reader channel")
	}

	man := &Manager{
		hubs: make(map[string]hub.Hub),
	}
	for r := range configReaders {
		h, err := buildHub(r)
		if err != nil {
			return nil, err
		}

		name := h.Name()
		if _, ok := man.hubs[name]; ok {
			return nil, fmt.Errorf("IO hub name collision [%s]", name)
		}
		man.hubs[name] = h
	}

	return man, nil
}

// NewIOConfig returns a function for creating new IO config objects
func (m *Manager) NewIOConfig(hubName string) func() interface{} {
	h, ok := m.hubs[hubName]
	if !ok || h == nil {
		return nil
	}
	return h.NewConfig
}

// Register registers a station IO through the named hub
func (m *Manager) Register(hubName string, infoList []*hub.IORegistrationInfo) error {
	h, ok := m.hubs[hubName]
	if !ok || h == nil {
		return fmt.Errorf("registering IO with non-existent hub [%s]", hubName)
	}

	for _, info := range infoList {
		if err := h.Register(info); err != nil {
			return fmt.Errorf(
				"error registering handler func [%s] of executor [%s] to IO hub [%s] as [%s]: %w",
				info.Function, info.Executor, hubName, info.Name, err,
			)
		}
	}
	return nil
}

// StartAll starts all IO hubs
func (m *Manager) StartAll() error {
	log.Println("starting IO hubs")
	for name, h := range m.hubs {
		log.Printf("starting IO hub [%s]\n", name)
		if err := h.Start(); err != nil {
			return fmt.Errorf("failed to start IO hub [%s]: %w", name, err)
		}
	}
	return nil
}

// ShutDownAll gracefully shuts down all IO hubs
func (m *Manager) ShutDownAll(ctx context.Context) {
	wg := sync.WaitGroup{}
	defer wg.Wait()

	log.Println("shutting down IO hubs")
	wg.Add(len(m.hubs))
	for n, h := range m.hubs {
		go func(name string, hub hub.Hub) {
			defer wg.Done()
			log.Printf("shutting down IO hub [%s]\n", name)
			hub.ShutDown(ctx)
			log.Printf("sucessfully shut down IO hub [%s]\n", name)
		}(n, h)
	}
}

// Hub returns the named hub
func (m *Manager) Hub(name string) hub.Hub {
	return m.hubs[name]
}

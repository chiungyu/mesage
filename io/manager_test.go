package io

import (
	"context"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/io/hub"
	"gitlab.com/chiungyu/mesage/io/testutils"
)

func TestNewManager(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })

	{ // empty config
		configReaders := make(chan io.Reader, 1)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)
	}

	{ // nil config reader
		configReaders := make(chan io.Reader, 1)
		configReaders <- nil
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Nil(t, man)
		assert.EqualError(t, err, "nil hub config reader")
	}

	{ // empty config reader channel
		man, err := NewManager(nil)
		assert.Nil(t, man)
		assert.EqualError(t, err, "nil hub config reader channel")
	}

	{ // good case
		hubNames := []string{"hub0", "hub1", "hub2"}
		configReaders := make(chan io.Reader, len(hubNames))
		for _, hubName := range hubNames {
			configReaders <- strings.NewReader(fmt.Sprintf(`
--- # config metadata
name: %s
type: mock
--- # config
value: 5
`, hubName))
		}
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)

		for _, hubName := range hubNames {
			hub, ok := man.Hub(hubName).(*testutils.MockHub)
			assert.True(t, ok)
			assert.NotNil(t, hub)
			assert.Equal(t, hubName, hub.Name())
			assert.Equal(t, testutils.MockHubType, hub.Type())
			assert.NotNil(t, man.NewIOConfig(hubName))
			assert.False(t, hub.Started)
			assert.False(t, hub.Close)
		}

		assert.Nil(t, man.Hub("non-existent"))
		assert.Nil(t, man.NewIOConfig("non-existent"))
	}

	{ // build error
		config := `
--- # config metadata
name: badHubInstance
type: mock
--- # config
buildError: some build error
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err,
			"failed to build IO hub: failed to build hub [badHubInstance] with builder [mock]: some build error")
		assert.Nil(t, man)
	}

	{ // unexpected trailing object in config
		config := `
--- # config metadata
name: one
type: mock
--- # config
value: 5
--- # trailing object
name: extra
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err, "unexpected config input, expecting EOF")
		assert.Nil(t, man)
	}

	{ // not yaml metadata
		config := "badConfig\n"
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to parse IO hub config"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // not yaml config
		config := `
--- # config metadata
name: test
type: mock
--- # config
notYAML
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build IO hub: error parsing hub [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // config type mismatch
		config := `
--- # config metadata
name: test
type: mock
--- # config
value: -42 # expecting uint
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build IO hub: error parsing hub [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // unexpected config field
		config := `
--- # config metadata
name: test
type: mock
--- # config
evil: 666 # no such field
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build IO hub: error parsing hub [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // hub name collision
		configReaders := make(chan io.Reader, 2)
		dupeConfig := `
--- # config metadata
name: dupe
type: mock
--- # config
value: 8
`
		configReaders <- strings.NewReader(dupeConfig)
		configReaders <- strings.NewReader(dupeConfig)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err, "IO hub name collision [dupe]")
		assert.Nil(t, man)
	}
}

func TestStartAndShutDown(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })

	{
		hubNames := []string{"hub0", "hub1", "hub2"}
		configReaders := make(chan io.Reader, len(hubNames))
		for _, hubName := range hubNames {
			configReaders <- strings.NewReader(fmt.Sprintf(`
--- # config metadata
name: %s
type: mock
--- # config
value: 5
`, hubName))
		}
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)

		hubs := make([]*testutils.MockHub, len(hubNames))
		for i, hubName := range hubNames {
			hub, ok := man.Hub(hubName).(*testutils.MockHub)
			assert.True(t, ok)
			assert.NotNil(t, hub)
			assert.False(t, hub.Started)
			assert.False(t, hub.Close)
			hubs[i] = hub
		}

		err = man.StartAll()
		assert.NoError(t, err)
		for _, h := range hubs {
			assert.True(t, h.Started)
		}

		man.ShutDownAll(context.Background())
		for _, h := range hubs {
			assert.True(t, h.Close)
		}
	}

	{ // non-nil error from Start call
		configReaders := make(chan io.Reader, 1)
		config := `
--- # config metadata
name: withStartError
type: mock
--- # config
startError: some start error
`
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)

		err = man.StartAll()
		assert.EqualError(t, err, "failed to start IO hub [withStartError]: some start error")
	}
}

func TestRegister(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterHubBuilderOrDie("mock", new(testutils.MockBuilder)) })

	configReaders := make(chan io.Reader, 1)
	configReaders <- strings.NewReader(`
--- # config metadata
name: test
type: mock
--- # config
value: 5
`)
	close(configReaders)

	man, err := NewManager(configReaders)
	assert.NoError(t, err)
	assert.NotNil(t, man)

	h, ok := man.Hub("test").(*testutils.MockHub)
	assert.True(t, ok)
	assert.NotNil(t, h)

	mockHandleFunc := func(context.Context, commons.Values) (commons.Values, error) { return nil, nil }

	{ // Good case
		err := man.Register(
			"test",
			[]*hub.IORegistrationInfo{
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: &testutils.MockIOConfig{Value: 5}},
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: &testutils.MockIOConfig{Value: 8}},
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: &testutils.MockIOConfig{Value: 17}},
			})
		assert.NoError(t, err)
	}

	{
		err := man.Register("non-ex", []*hub.IORegistrationInfo{})
		assert.EqualError(t, err, "registering IO with non-existent hub [non-ex]")
	}

	{ // IO registration error
		err := man.Register(
			"test",
			[]*hub.IORegistrationInfo{
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: &testutils.MockIOConfig{Error: "error message"}},
			})
		assert.EqualError(t, err, "error registering handler func [func0] of executor [testExecutor] to IO hub [test] as [testIO]: error message")

	}

	{ // IO config type error
		err := man.Register(
			"test",
			[]*hub.IORegistrationInfo{
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: 32},
			})
		assert.EqualError(t, err, "error registering handler func [func0] of executor [testExecutor] to IO hub [test] as [testIO]: bad config type or nil config")
	}

	{ // nil config
		var config *testutils.MockIOConfig = nil
		err := man.Register(
			"test",
			[]*hub.IORegistrationInfo{
				{Name: "testIO", Executor: "testExecutor", Function: "func0", Handle: mockHandleFunc, Config: config},
			})
		assert.EqualError(t, err, "error registering handler func [func0] of executor [testExecutor] to IO hub [test] as [testIO]: bad config type or nil config")
	}
}

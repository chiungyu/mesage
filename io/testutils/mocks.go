package testutils

import (
	"context"
	"fmt"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/io/hub"
)

// MockHubType for tests
const MockHubType = "mock"

// MockBuilder for building MockHub
type MockBuilder int

// MockHubConfig for building MockHub
type MockHubConfig struct {
	Value      uint32 `yaml:"value"`
	BuildError string `yaml:"buildError"`
	StartError string `yaml:"startError"`
}

// NewConfig implemetation
func (*MockBuilder) NewConfig() interface{} {
	return new(MockHubConfig)
}

// Build implementation
func (*MockBuilder) Build(name string, config interface{}) (hub.Hub, error) {
	c, ok := config.(*MockHubConfig)
	if !ok || c == nil {
		return nil, commons.ErrorBadConfigType
	}

	if len(c.BuildError) > 0 {
		return nil, fmt.Errorf(c.BuildError)
	}

	if len(c.StartError) > 0 {
		return &MockHub{name: name, startError: fmt.Errorf(c.StartError)}, nil
	}
	return &MockHub{name: name}, nil
}

// MockHub for tests
type MockHub struct {
	name    string
	Started bool
	Close   bool

	startError error
}

// MockIOConfig for registering IO against MockHub
type MockIOConfig struct {
	Value uint32 `yaml:"value"`
	Error string `yaml:"error"`
}

// NewConfig implemetation
func (m *MockHub) NewConfig() interface{} {
	return new(MockIOConfig)
}

// Register implementation
func (m *MockHub) Register(info *hub.IORegistrationInfo) error {
	if info == nil {
		return commons.ErrorBadConfigType
	}
	c, ok := info.Config.(*MockIOConfig)
	if !ok || c == nil {
		return commons.ErrorBadConfigType
	}

	if len(c.Error) > 0 {
		return fmt.Errorf(c.Error)
	}
	return nil
}

// Start implementation
func (m *MockHub) Start() error {
	m.Started = true
	return m.startError
}

// ShutDown implementation
func (m *MockHub) ShutDown(ctx context.Context) {
	m.Close = true
}

// Name implementation
func (m *MockHub) Name() string {
	return m.name
}

// Type implementation
func (m *MockHub) Type() string {
	return MockHubType
}

package testutils

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/io/hub"
)

func TestMockHub(t *testing.T) {
	builder := new(MockBuilder)
	assert.Implements(t, new(hub.Builder), builder)

	// Good case
	{
		config, ok := builder.NewConfig().(*MockHubConfig)
		assert.True(t, ok)

		h, err := builder.Build("test", config)
		assert.NoError(t, err)
		assert.Implements(t, new(hub.Hub), h)

		ioConfig, ok := h.NewConfig().(*MockIOConfig)
		assert.True(t, ok)

		assert.NoError(t, h.Register(&hub.IORegistrationInfo{Config: ioConfig}))

		mockHub, ok := h.(*MockHub)
		assert.True(t, ok)
		assert.NoError(t, h.Start())
		assert.True(t, mockHub.Started)
		h.ShutDown(context.Background())
		assert.True(t, mockHub.Close)
		assert.Equal(t, "test", h.Name())
		assert.Equal(t, "mock", h.Type())
	}

	// Bad config objects
	{
		_, err := builder.Build("test", nil)
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}
	{
		_, err := builder.Build("test", new(int))
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}

	// Build error
	{
		config, ok := builder.NewConfig().(*MockHubConfig)
		assert.True(t, ok)

		config.BuildError = "some error"
		_, err := builder.Build("test", config)
		assert.EqualError(t, err, "some error")
	}

	// Start error
	{
		config, ok := builder.NewConfig().(*MockHubConfig)
		assert.True(t, ok)

		config.StartError = "some error"
		h, err := builder.Build("test", config)
		assert.NoError(t, err)
		assert.Implements(t, new(hub.Hub), h)

		assert.EqualError(t, h.Start(), "some error")
	}

	// Bad IO registration
	{
		config, ok := builder.NewConfig().(*MockHubConfig)
		assert.True(t, ok)

		h, err := builder.Build("test", config)
		assert.NoError(t, err)
		assert.Implements(t, new(hub.Hub), h)

		err = h.Register(nil)
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())

		err = h.Register(&hub.IORegistrationInfo{Config: nil})
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())

		ioConfig, ok := h.NewConfig().(*MockIOConfig)
		assert.True(t, ok)

		ioConfig.Error = "some error"
		err = h.Register(&hub.IORegistrationInfo{Config: ioConfig})
		assert.EqualError(t, err, "some error")
	}
}

package service

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/example/keys"
)

// Config struct
type Config struct {
	DefaultMode string `yaml:"defaultMode"`
}

// Service implementation
type Service struct {
	name   string
	config *Config
}

// Name implementaion
func (s *Service) Name() string {
	return s.name
}

// Type implementation
func (*Service) Type() string {
	return builderName
}

// Print prints the value in the specified mode.
func (s *Service) Print(ctx context.Context, mode string, values commons.Values) {
	if len(mode) == 0 {
		mode = s.config.DefaultMode
	}

	var fn func(string) string
	if mode == "CAP" {
		fmt.Printf("[PRINT: %s] CAP MODE\n", ctx.Value(keys.RequestIDKey))
		fn = strings.ToUpper
	} else if mode == "low" {
		fmt.Printf("[PRINT: %s] low mode\n", ctx.Value(keys.RequestIDKey))
		fn = strings.ToLower
	} else {
		fmt.Printf("[PRINT: %s] Unknown Mode\n", ctx.Value(keys.RequestIDKey))
		return
	}
	for k, v := range values {
		if sv, ok := v.(string); ok {
			fmt.Printf("> %s: %s\n", fn(k), fn(sv))
		} else {
			fmt.Printf("> %s: %v\n", fn(k), v)
		}
	}
}

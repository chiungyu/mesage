package service

import (
	"fmt"

	"gitlab.com/chiungyu/mesage/services"
	"gitlab.com/chiungyu/mesage/services/service"
)

const builderName = "mock"

type builder int

// NewConfig implementation
func (*builder) NewConfig() interface{} {
	return new(Config)
}

// Build implementation
func (*builder) Build(name string, configInterface interface{}) (service.Service, error) {
	fmt.Println(configInterface)
	config, ok := configInterface.(*Config)
	if !ok {
		return nil, fmt.Errorf("incompatible config")
	}

	return &Service{
		name:   name,
		config: config,
	}, nil
}

func init() {
	services.RegisterBuilderOrDie(builderName, new(builder))
}

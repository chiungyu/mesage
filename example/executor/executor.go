package executor

import (
	"context"
	"fmt"
	"reflect"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/example/service"
)

// AddConfig struct
type AddConfig struct {
	Add int `yaml:"add"`
}

// MulConfig struct
type MulConfig struct {
	Mul int `yaml:"mul"`
}

// PrintConfig struct
type PrintConfig struct {
	Mode string `yaml:"mode"`
}

// Config struct
type Config struct {
	Add int `yaml:"add"`
	Mul int `yaml:"mul"`
}

// Executor implementation
type Executor struct {
	name    string
	config  *Config
	printer *service.Service
}

func copy(in commons.Values) (commons.Values, error) {
	name, _ := in["inName"].(string)
	color, _ := in["inColor"].(string)
	score, _ := in["inScore"].(int)
	if name == "" {
		return nil, fmt.Errorf("missing name")
	}
	return commons.Values{
		"outName":  name,
		"outColor": color,
		"outScore": score,
	}, nil
}

// HandleFuncSpecs implementation
func (e *Executor) HandleFuncSpecs() map[string]reflect.Type {
	return map[string]reflect.Type{
		"add":   reflect.TypeOf(new(AddConfig)),
		"mul":   reflect.TypeOf(new(MulConfig)),
		"print": reflect.TypeOf(new(PrintConfig)),
	}
}

// NewHandleFuncConfig implementation
func (e *Executor) NewHandleFuncConfig(name string) interface{} {
	return new(Config)
}

// HandleFunc implementation
func (e *Executor) HandleFunc(name string, config interface{}) (commons.Handle, error) {
	switch name {
	case "mul":
		mul := e.config.Mul
		if c, ok := config.(*MulConfig); ok && c != nil {
			mul = c.Mul
		}
		return func(ctx context.Context, in commons.Values) (commons.Values, error) {
			out, err := copy(in)
			if err != nil {
				return nil, err
			}
			out["outScore"] = out["outScore"].(int) * mul
			return out, nil
		}, nil
	case "add":
		add := e.config.Add
		if c, ok := config.(*AddConfig); ok && c != nil {
			add = c.Add
		}
		return func(ctx context.Context, in commons.Values) (commons.Values, error) {
			out, err := copy(in)
			if err != nil {
				return nil, err
			}
			out["outScore"] = out["outScore"].(int) + add
			return out, nil
		}, nil
	case "print":
		mode := ""
		if c, ok := config.(*PrintConfig); ok && c != nil {
			mode = c.Mode
		}
		return func(ctx context.Context, in commons.Values) (commons.Values, error) {
			e.printer.Print(ctx, mode, in)
			return copy(in)
		}, nil
	default:
		return nil, nil // No such function
	}
}

// Name implementaion
func (e *Executor) Name() string {
	return e.name
}

// Type implementation
func (*Executor) Type() string {
	return builderName
}

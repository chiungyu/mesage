package executor

import (
	"fmt"

	psi "gitlab.com/chiungyu/mesage/example/service"
	"gitlab.com/chiungyu/mesage/executors"
	"gitlab.com/chiungyu/mesage/executors/executor"
	"gitlab.com/chiungyu/mesage/services/service"
)

const builderName = "mock"

type builder int

// NewConfig implementation
func (*builder) NewConfig() interface{} {
	return new(Config)
}

// Build implementation
func (*builder) Build(name string, configInterface interface{}, services map[string]service.Service) (executor.Executor, error) {
	config, ok := configInterface.(*Config)
	if !ok {
		return nil, fmt.Errorf("incompatible config")
	}

	printer, ok := services["printer"]
	if !ok || printer == nil {
		return nil, fmt.Errorf("missing printer service")
	}
	printerServiceImpl, ok := printer.(*psi.Service)
	if !ok {
		return nil, fmt.Errorf("bad printer service type")
	}

	return &Executor{
		name:    name,
		config:  config,
		printer: printerServiceImpl,
	}, nil
}

func init() {
	executors.RegisterBuilderOrDie(builderName, new(builder))
}

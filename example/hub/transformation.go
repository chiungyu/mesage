package hub

import (
	"strings"

	"gitlab.com/chiungyu/mesage/commons"
)

const configSuffix = "|hub"

func transform(t map[string]string, config *Config, fromValues commons.Values) (commons.Values, error) {
	toValues := make(commons.Values, len(t))
	for to, from := range t {
		if strings.HasSuffix(from, configSuffix) {
			if v := config.get(from[:len(from)-len(configSuffix)]); v != nil {
				toValues[to] = v
			}
		} else if v := fromValues[from]; v != nil {
			toValues[to] = v
		}
	}
	return toValues, nil
}

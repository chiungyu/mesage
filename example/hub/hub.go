package hub

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/rs/xid"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/example/keys"
	"gitlab.com/chiungyu/mesage/io/hub"
)

// IOConfig for hub IOs
type IOConfig struct {
	InputTransformation  map[string]string `yaml:"ioInputTransformation"`
	OutputTransformation map[string]string `yaml:"ioOutputTransformation"`
}

// IO definition
type IO struct {
	handle               commons.Handle
	inputTransformation  map[string]string
	outputTransformation map[string]string
}

// Config for hub
type Config struct {
	Color string `yaml:"color"`
	Taste string `yaml:"taste"`
}

func (c *Config) get(key string) interface{} {
	switch key {
	case "color":
		return c.Color
	case "taste":
		return c.Taste
	default:
		return ""
	}
}

// Hub implementation
type Hub struct {
	name       string
	config     *Config
	ioRegistry map[string]*IO

	mu      sync.Mutex
	started bool
	closed  bool
	wg      sync.WaitGroup
}

// NewConfig implementation for IO registeration
func (h *Hub) NewConfig() interface{} {
	return new(IOConfig)
}

// Register implementation
func (h *Hub) Register(info *hub.IORegistrationInfo) error {
	// Start should be called after all Register calls
	h.mu.Lock()
	defer h.mu.Unlock()

	if h.started {
		return fmt.Errorf("register is disallowed after the hub is started")
	}

	name := fmt.Sprintf("%s|%s", info.Executor, info.Name)
	if _, ok := h.ioRegistry[name]; ok {
		return fmt.Errorf("io name collision: %s", name)
	}

	config, ok := info.Config.(*IOConfig)
	if !ok {
		return fmt.Errorf("imcompatible io config")
	}

	log.Printf("new io %s registered to hub %s", name, h.name)
	h.ioRegistry[name] = &IO{
		info.Handle,
		config.InputTransformation,
		config.OutputTransformation}
	return nil
}

// Start implementation
func (h *Hub) Start() error {
	h.mu.Lock()
	defer h.mu.Unlock()
	if h.started {
		return fmt.Errorf("hub already started")
	}
	h.started = true
	return nil
}

// ShutDown implementation
func (h *Hub) ShutDown(context.Context) {
	h.mu.Lock()
	defer h.mu.Unlock()
	h.closed = true

	h.wg.Wait() // Wait until all ongoing IOs are completed
}

// Name implementaion
func (h *Hub) Name() string {
	return h.name
}

// Type implementation
func (*Hub) Type() string {
	return builderName
}

// Returns error if hub closed; otherwise adds the received job to work group
func (h *Hub) receive() error {
	h.mu.Lock()
	defer h.mu.Unlock()

	if !h.started || h.closed {
		return fmt.Errorf("hub not started or hub closed")
	}

	h.wg.Add(1)
	return nil
}

// Handle handles the IO
func (h *Hub) Handle(ioName string, input commons.Values) (commons.Values, error) {
	if err := h.receive(); err != nil {
		return nil, err
	}
	defer h.wg.Done()

	ctx := context.WithValue(context.Background(), keys.RequestIDKey, xid.New().String())

	i, ok := h.ioRegistry[ioName]
	if !ok {
		return nil, fmt.Errorf("io not registered [%s]", ioName)
	}

	tIn, err := transform(i.inputTransformation, h.config, input)
	if err != nil {
		return nil, fmt.Errorf("error processing input %w", err)
	}
	output, err := i.handle(ctx, tIn)
	if err != nil {
		return nil, fmt.Errorf("error handling input %w", err)
	}
	tOut, err := transform(i.outputTransformation, h.config, output)
	if err != nil {
		return nil, fmt.Errorf("error processing output %w", err)
	}
	return tOut, nil
}

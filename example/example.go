package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/chiungyu/mesage/commons"
	_ "gitlab.com/chiungyu/mesage/example/executor"
	"gitlab.com/chiungyu/mesage/example/hub"
	_ "gitlab.com/chiungyu/mesage/example/service"
	"gitlab.com/chiungyu/mesage/executors"
	sio "gitlab.com/chiungyu/mesage/io"
	"gitlab.com/chiungyu/mesage/services"
)

func newHubManager() *sio.Manager {
	configReaders := make(chan io.Reader, 16)
	for _, s := range []string{"Lemon", "Mongo"} {
		f, err := os.Open(fmt.Sprintf("example/configs/hubs/example%sHub.yaml", s))
		if err != nil {
			log.Fatal("error opening config file", err)
		}
		configReaders <- f
	}
	close(configReaders)

	man, err := sio.NewManager(configReaders)
	if err != nil {
		log.Fatalln("error creating new hub manager:", err)
	}
	return man
}

func newServiceManager() *services.Manager {
	configReaders := make(chan io.Reader, 16)
	f, err := os.Open("example/configs/services/exampleService.yaml")
	if err != nil {
		log.Fatal("error opening config file", err)
	}
	configReaders <- f
	close(configReaders)

	man, err := services.NewManager(configReaders)
	if err != nil {
		log.Fatalln("error creating new service manager:", err)
	}
	return man
}

func newExecutorManager(hubMan *sio.Manager, serviceMan *services.Manager) *executors.Manager {
	configReaders := make(chan io.Reader, 16)
	f, err := os.Open("example/configs/executors/exampleExecutor.yaml")
	if err != nil {
		log.Fatal("error opening config file", err)
	}
	configReaders <- f
	close(configReaders)

	man, err := executors.NewManager(configReaders, hubMan, serviceMan)
	if err != nil {
		log.Fatalln("error creating new hub manager:", err)
	}
	return man
}

func main() {
	hubMan := newHubManager()
	serviceMan := newServiceManager()
	_ = newExecutorManager(hubMan, serviceMan)

	hubMan.StartAll()
	defer hubMan.ShutDownAll(context.Background())

	in := commons.Values{
		"name":  "test",
		"score": 3,
	}
	log.Println("input:", in)

	for _, s := range []string{"Lemon", "Mongo"} {
		log.Println("HUB", s)

		h := hubMan.Hub(fmt.Sprintf("example%sHub", s)).(*hub.Hub)

		out, err := h.Handle("exampleExecutor|add", in)
		log.Println("add output:", out, err)

		out, err = h.Handle("exampleExecutor|addMore", in)
		log.Println("addMore output:", out, err)

		out, err = h.Handle("exampleExecutor|mul", in)
		log.Println("mul output:", out, err)

		out, err = h.Handle("exampleExecutor|div", in)
		log.Println("div output:", out, err)

		out, err = h.Handle("exampleExecutor|print", in)
		log.Println("print output:", out, err)
	}
}

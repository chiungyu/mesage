module gitlab.com/chiungyu/mesage

go 1.13

require (
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.2.8
)

package commons

import "fmt"

// ErrorBadConfigType for bad hub/executor/service configs
var ErrorBadConfigType = fmt.Errorf("bad config type or nil config")

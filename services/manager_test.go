package services

import (
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/services/testutils"
)

func TestNewManager(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

	{ // empty config
		configReaders := make(chan io.Reader, 1)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)
	}

	{ // nil config reader
		configReaders := make(chan io.Reader, 1)
		configReaders <- nil
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Nil(t, man)
		assert.EqualError(t, err, "nil service config reader")
	}

	{ // empty config reader channel
		man, err := NewManager(nil)
		assert.Nil(t, man)
		assert.EqualError(t, err, "nil service config reader channel")
	}

	{ // good case
		serviceNames := []string{"s0", "s1", "s2"}
		configReaders := make(chan io.Reader, len(serviceNames))
		for _, serviceName := range serviceNames {
			configReaders <- strings.NewReader(fmt.Sprintf(`
--- # config metadata
name: %s
type: mock
--- # config
value: 5
`, serviceName))
		}
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.NoError(t, err)
		assert.NotNil(t, man)

		for _, serviceName := range serviceNames {
			s, ok := man.Service(serviceName).(*testutils.MockService)
			assert.True(t, ok)
			assert.NotNil(t, s)
			assert.Equal(t, serviceName, s.Name())
			assert.Equal(t, testutils.MockServiceType, s.Type())
		}

		assert.Nil(t, man.Service("non-existent"))
	}
	{ // build error
		config := `
--- # config metadata
name: badServiceInstance
type: mock
--- # config
error: some build error
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err,
			"failed to build service: failed to build service [badServiceInstance] with builder [mock]: some build error")
		assert.Nil(t, man)
	}

	{ // unexpected trailing object in config
		config := `
--- # config metadata
name: one
type: mock
--- # config
value: 5
--- # trailing object
name: extra
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err, "unexpected config input, expecting EOF")
		assert.Nil(t, man)
	}

	{ // not yaml metadata
		config := "notYAML\n"
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to parse service config"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // not yaml config
		config := `
--- # config metadata
name: test
type: mock
--- # config
notYAML
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build service: error parsing service [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // config type mismatch
		config := `
--- # config metadata
name: test
type: mock
--- # config
value: -5 # expecting uint
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build service: error parsing service [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // unexpected config field
		config := `
--- # config metadata
name: test
type: mock
--- # config
score: -5 # no such field
`
		configReaders := make(chan io.Reader, 1)
		configReaders <- strings.NewReader(config)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.Error(t, err)
		es := "failed to build service: error parsing service [test] config with builder [mock]"
		assert.Equal(t, es, err.Error()[:len(es)])
		assert.Nil(t, man)
	}

	{ // service name collision
		configReaders := make(chan io.Reader, 2)
		dupeConfig := `
--- # config metadata
name: dupe
type: mock
--- # config
value: 8
`
		configReaders <- strings.NewReader(dupeConfig)
		configReaders <- strings.NewReader(dupeConfig)
		close(configReaders)

		man, err := NewManager(configReaders)
		assert.EqualError(t, err, "service name collision [dupe]")
		assert.Nil(t, man)
	}
}

package testutils

import (
	"fmt"

	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/services/service"
)

// MockServiceType for tests
const MockServiceType = "mock"

// MockBuilder for building MockService
type MockBuilder int

// MockServiceConfig for building MockService
type MockServiceConfig struct {
	Value      uint32 `yaml:"value"`
	BuildError string `yaml:"error"`
}

// NewConfig implemetation
func (*MockBuilder) NewConfig() interface{} {
	return new(MockServiceConfig)
}

// Build implementation
func (*MockBuilder) Build(name string, config interface{}) (service.Service, error) {
	c, ok := config.(*MockServiceConfig)
	if !ok || c == nil {
		return nil, commons.ErrorBadConfigType
	}

	if len(c.BuildError) > 0 {
		return nil, fmt.Errorf(c.BuildError)
	}
	return &MockService{name: name}, nil
}

// MockService for tests
type MockService struct {
	name string
}

// Name implementation
func (m *MockService) Name() string {
	return m.name
}

// Type implementation
func (m *MockService) Type() string {
	return MockServiceType
}

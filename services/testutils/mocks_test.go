package testutils

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/mesage/commons"
	"gitlab.com/chiungyu/mesage/services/service"
)

func TestMock(t *testing.T) {
	builder := new(MockBuilder)
	assert.Implements(t, new(service.Builder), builder)

	// Good case
	{
		config, ok := builder.NewConfig().(*MockServiceConfig)
		assert.True(t, ok)

		s, err := builder.Build("test", config)
		assert.NoError(t, err)
		assert.Implements(t, new(service.Service), s)

		assert.Equal(t, "test", s.Name())
		assert.Equal(t, "mock", s.Type())
	}

	// Bad config objects
	{
		_, err := builder.Build("test", nil)
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}
	{
		_, err := builder.Build("test", new(int))
		assert.EqualError(t, err, commons.ErrorBadConfigType.Error())
	}

	// Build error
	{
		config, ok := builder.NewConfig().(*MockServiceConfig)
		assert.True(t, ok)

		config.BuildError = "some error"
		_, err := builder.Build("test", config)
		assert.EqualError(t, err, "some error")
	}
}

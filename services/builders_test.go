package services

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/chiungyu/mesage/services/service"
	"gitlab.com/chiungyu/mesage/services/testutils"
)

func TestRegisterServiceBuilder(t *testing.T) {
	{ // nil builder
		assert.PanicsWithValue(t,
			"nil service builder [nilBuilder]",
			func() { RegisterBuilderOrDie("nilBuilder", nil) })
		assert.Equal(t, map[string]service.Builder{}, builderRegistry)
	}

	{
		// normal register calls
		assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })
		assert.NotPanics(t, func() { RegisterBuilderOrDie("mockAlt", new(testutils.MockBuilder)) })
		assert.Equal(t, 2, len(builderRegistry))
		assert.NotNil(t, builderRegistry["mock"])
		assert.NotNil(t, builderRegistry["mockAlt"])

		// builder name collision
		assert.PanicsWithValue(t,
			"service builder name collision [mock]",
			func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

		ClearBuilders()
	}
}

func TestBuildService(t *testing.T) {
	defer ClearBuilders()
	assert.NotPanics(t, func() { RegisterBuilderOrDie("mock", new(testutils.MockBuilder)) })

	{
		configParser := func(interface{}) error { return nil }
		s, err := Build("testService", "mock", configParser)
		assert.NoError(t, err)
		assert.NotNil(t, s)
		assert.Equal(t, "testService", s.Name())
		assert.Equal(t, testutils.MockServiceType, s.Type())
	}

	{
		buildError := "some build error"
		configParser := func(c interface{}) error {
			config, ok := c.(*testutils.MockServiceConfig)
			assert.True(t, ok)
			assert.NotNil(t, config)
			config.BuildError = buildError
			return nil
		}

		s, err := Build("testService", "mock", configParser)
		assert.Nil(t, s)
		assert.EqualError(t, err,
			fmt.Sprintf("failed to build service [testService] with builder [mock]: %s", buildError))
	}

	{
		s, err := Build("testService", "mock", nil)
		assert.Nil(t, s)
		assert.EqualError(t, err, "nil config parser for service [testService]")
	}

	{
		configError := "some config error"
		configParser := func(c interface{}) error {
			return fmt.Errorf(configError)
		}

		s, err := Build("testService", "mock", configParser)
		assert.Nil(t, s)
		assert.EqualError(t, err,
			fmt.Sprintf("error parsing service [testService] config with builder [mock]: %s", configError))
	}

	{
		configParser := func(interface{}) error { return nil }
		s, err := Build("testService", "nonExistent", configParser)
		assert.Nil(t, s)
		assert.EqualError(t, err, "non-existent service builder [nonExistent]")
	}
}
